/* GnomeInfoWindow - Show properties of the file being shared.
   Copyright (C) 2003 Mark J. Wielaard

   This file is part of Snark.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
   In addition, as a special exception, the copyright holders of Snark give
   you permission to combine Snark with free software programs or libraries
   that are released under the GNU LGPL and with any code released under
   the Apache Software License, version 1.0, 1.1 or 2.0. You may copy and
   distribute such a system following the terms of the GNU GPL for Snark
   and the following licenses of the other code concerned, provided that
   you include the source code of that other code when and as the GNU GPL
   requires distribution of source code.

   Note that people who make modified versions of Snark are not
   obligated to grant this special exception for their modified versions;
   it is their choice whether to do so. The GNU General Public License
   gives permission to release a modified version without this exception;
   this exception also makes it possible to release a modified version
   which carries forward this exception.
 */

package org.klomp.snark;

import org.gnu.glade.LibGlade;
import org.gnu.gtk.Button;
import org.gnu.gtk.Dialog;
import org.gnu.gtk.Label;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * Show a Gnome window with the properties of the file being shared.
 * 
 * @author Mark Wielaard <mark@klomp.org>
 * @author Ben Konrath <ben@bagu.org>
 */
public class SnarkGnomeProperties implements ButtonListener, LifeCycleListener {

	private Dialog properties;
	private Button closeButton;
	private Button peersButton;
	private Label peersLabel;

	private final String GLADE_FILE = "data/snark-prop.glade";
	private LibGlade propertiesGlade;
	
	private SnarkGnomePeersList peersWindow;
	private int peers;


	public SnarkGnomeProperties() {
		peersWindow = new SnarkGnomePeersList();
	}

	private void fillProperties() {
        
		// Torrent file name
		Label file = (Label) propertiesGlade.getWidget("file");
		file.setLabel(Snark.meta.getName());
		// Full URL
		Label torrentName = (Label) propertiesGlade.getWidget("torrentName");
		torrentName.setLabel(Snark.torrent);
		// Tracker that we are using
		Label trackerName = (Label) propertiesGlade.getWidget("trackerName");
		trackerName.setLabel(Snark.meta.getAnnounce());
		// Pieces
		Label piecesTotal = (Label) propertiesGlade.getWidget("piecesTotal");
		piecesTotal.setLabel(String.valueOf(Snark.meta.getPieces()));
		// Piece size
		Label pieceSize = (Label) propertiesGlade.getWidget("pieceSize");
		pieceSize.setLabel(Snark.meta.getPieceLength(0) / 1024 + " KB");
		// Total length
		Label totalSize = (Label) propertiesGlade.getWidget("totalSize");
		totalSize.setLabel(Snark.meta.getTotalLength() / (1024 * 1024) + " MB");
		// Peers
		// set to 0 so that it gets updated
		peers = 0;
		peersLabel = (Label) propertiesGlade.getWidget("peersLabel");
		if (Snark.coordinator != null)
			update(Snark.coordinator.getPeers());
		// Buttons
		closeButton = (Button) propertiesGlade.getWidget("closeButton");
		closeButton.addListener((ButtonListener) this);
		peersButton = (Button) propertiesGlade.getWidget("peersButton");
		peersButton.addListener((ButtonListener) this);
	}

	/**
	 * We don't have to do anything for these types of events.
	 * 
	 * @see LifeCycleListener
	 */
	public void lifeCycleEvent(LifeCycleEvent event) {
	}
	
	/**
	 * Handles Life Cycle events (Window close or delete).
	 * 
	 * @see LifeCycleListener
	 */
	public boolean lifeCycleQuery(LifeCycleEvent event) {
		if (event.isOfType(LifeCycleEvent.Type.DELETE)
				|| event.isOfType(LifeCycleEvent.Type.DESTROY)) {
			properties.destroy();
			properties = null;
			propertiesGlade = null;
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Handles buttons (Peers, Close).
	 */
	public void buttonEvent(ButtonEvent event) {
		if (event.isOfType(ButtonEvent.Type.CLICK)) {
			Object source = event.getSource();
			if (source.equals(peersButton))
				peersWindow.show();
			else if (source.equals(closeButton)) {
				properties.destroy();
				properties = null;
				propertiesGlade = null;
			} else
				System.err.println("Unknow event: " + event + " from source: "
						+ source);
		}
	}

	void show() {
		if (properties != null) {
			properties.present();
			return;
		}

		try {
			propertiesGlade = new LibGlade(GLADE_FILE, this, "properties");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		properties = (Dialog) propertiesGlade.getWidget("properties");
		properties.addListener((LifeCycleListener) this);
		fillProperties();

		properties.showAll();
	}

	// Update the number of peers and the peers window.
	void update(int peers) {
		if (properties != null && this.peers != peers) {
			this.peers = peers;
			peersLabel.setText(String.valueOf(peers));
		}
		peersWindow.update();
	}


}
