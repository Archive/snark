/* StaticSnark - Main snark startup class for staticly linking with gcj.
   Copyright (C) 2003 Mark J. Wielaard

   This file is part of Snark.
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
   
   In addition, as a special exception, the copyright holders of Snark give
   you permission to combine Snark with free software programs or libraries
   that are released under the GNU LGPL and with any code released under
   the Apache Software License, version 1.0, 1.1 or 2.0. You may copy and
   distribute such a system following the terms of the GNU GPL for Snark
   and the following licenses of the other code concerned, provided that
   you include the source code of that other code when and as the GNU GPL
   requires distribution of source code.

   Note that people who make modified versions of Snark are not
   obligated to grant this special exception for their modified versions;
   it is their choice whether to do so. The GNU General Public License
   gives permission to release a modified version without this exception;
   this exception also makes it possible to release a modified version
   which carries forward this exception.
*/

package org.klomp.snark;

import java.security.Provider;
import java.security.Security;


/**
 * Main snark startup class for staticly linking with gcj.
 * It references some necessary classes that are normally loaded through
 * reflection.
 *
 * @author Mark Wielaard (mark@klomp.org)
 */
public class StaticSnark
{
  public static void main(String[] args)
  {
    // The GNU security provider is needed for SHA-1 MessageDigest checking.
    // So make sure it is available as a security provider.
    //Provider gnu = new gnu.java.security.provider.Gnu();
    //Security.addProvider(gnu);

    // And finally call the normal starting point.
    Snark.main(args);
  }
}
