/* SnarkGnome - Main snark program startup class which uses a Gnome UI.
   Copyright (C) 2003 Mark J. Wielaard

   This file is part of Snark.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
   In addition, as a special exception, the copyright holders of Snark give
   you permission to combine Snark with free software programs or libraries
   that are released under the GNU LGPL and with any code released under
   the Apache Software License, version 1.0, 1.1 or 2.0. You may copy and
   distribute such a system following the terms of the GNU GPL for Snark
   and the following licenses of the other code concerned, provided that
   you include the source code of that other code when and as the GNU GPL
   requires distribution of source code.

   Note that people who make modified versions of Snark are not
   obligated to grant this special exception for their modified versions;
   it is their choice whether to do so. The GNU General Public License
   gives permission to release a modified version without this exception;
   this exception also makes it possible to release a modified version
   which carries forward this exception.
 */

package org.klomp.snark;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.gnu.glade.GladeXMLException;
import org.gnu.glade.LibGlade;
import org.gnu.glib.Fireable;
import org.gnu.glib.Timer;
import org.gnu.gnome.App;
import org.gnu.gnome.AppBar;
import org.gnu.gnome.Program;
import org.gnu.gtk.AboutDialog;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Label;
import org.gnu.gtk.ProgressBar;
import org.gnu.gtk.event.GtkEvent;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * Main Snark program startup class that uses a Gnome UI.
 * 
 * @author Mark Wielaard (mark@klomp.org)
 */
public class SnarkGnome implements Runnable, StorageListener,
		CoordinatorListener, ShutdownListener, Fireable, LifeCycleListener {
	private static final String VERSION = "0.5.0";

	// How often to call the update time which calls fire() per second.
	static int UPDATE_TIMER = 2;

	private static SnarkShutdown snarkhook;

	private String[] args;
	private String name;

	private final String GLADE_FILE = "/home/ben/workspace/snark/data/snark.glade"; //FIXME
	private LibGlade appGlade;
	private LibGlade aboutGlade;

	private final App app;
	private final AboutDialog about;
	private final AppBar appbar;
	private final SnarkGnomeProperties propertiesWindow;

	private final Label torrentName;
	private Label downloadRate;
	private Label uploadRate;
	private Label piecesCollected;

	/**
	 * Creates the main SnarkGnome application.
	 */
	private SnarkGnome() throws IOException, FileNotFoundException,
			GladeXMLException {
		//FIXME: Big problem here
		// connect the signals and get references to the
		// widgets we care about for the main app
		//InputStream is =
		//ClassLoader.getSystemClassLoader().getResourceAsStream(GLADE_FILE);
		//appGlade = new LibGlade(is, this, null);
		appGlade = new LibGlade(GLADE_FILE, this, "main");
		app = (App) appGlade.getWidget("main");
		app.addListener((LifeCycleListener) this);
		appbar = (AppBar) appGlade.getWidget("appbar");
		appbar.setStatusText("Snark ready...");

		torrentName = (Label) appGlade.getWidget("torrentName");
		downloadRate = (Label) appGlade.getWidget("downloadRate");
		uploadRate = (Label) appGlade.getWidget("uploadRate");
		piecesCollected = (Label) appGlade.getWidget("piecesCollected");

		// TODO deal with the about dialog
		//aboutGlade = new LibGlade(is, this, null);
		aboutGlade = new LibGlade(GLADE_FILE, this, "about");
		about = (AboutDialog) aboutGlade.getWidget("about");
        
		// We are ready to show and tell!
		app.showAll();
		propertiesWindow = new SnarkGnomeProperties();

		// Check for progress and update the progress bar every half second.
		Timer timer = new Timer(1000 / UPDATE_TIMER, this);
		timer.start();
	}

	/**
	 * We don't care about these tiems of events.
	 *  
	 * @see LifeCycleListener
	 */
	public void lifeCycleEvent(LifeCycleEvent event) {
	}

	/**
	 * Handles Life Cycle events (main application window close or delete).
	 * 
	 * @see LifeCycleListener
	 */
	public boolean lifeCycleQuery(LifeCycleEvent event) {
		if (event.isOfType(LifeCycleEvent.Type.DELETE)
				|| event.isOfType(LifeCycleEvent.Type.DESTROY)) {
			on_quit_activate(null);
			return true;
		} else {
			return false;
		}
	}

	public void on_properties_activate(GtkEvent event) {
		propertiesWindow.show();
	}

	public void on_quit_activate(GtkEvent event) {
		activity = SHUTDOWN;
		if (snarkhook != null) {
			Runtime.getRuntime().removeShutdownHook(snarkhook);
			snarkhook.start();
		} else
			shutdown();
	}

	public void on_about_activate(GtkEvent event) {
		about.show();
	}

	// Called by the shutdown hook
	public void shutdown() {
		Gtk.mainQuit();
		System.exit(0);
	}

	// Creates the actual Snark object. Runs in the background
	public void run() {
		
		Snark.startSnark(this, this);
		
		snarkhook = new SnarkShutdown(Snark.storage, Snark.coordinator,
				Snark.acceptor, Snark.trackerclient, this);
		Runtime.getRuntime().addShutdownHook(snarkhook);
	}

	/**
	 * Starts snark with a Gnome UI.
	 */
	public static void main(String[] args) throws Exception {
		// Initialize Gnome libraries and handle common arguments.
		Program.initGnomeUI("Snark", VERSION, args);
		
		if (Snark.parseArguments(args) == false) {
			System.exit(-1);
		}
		
		// Setup the main application window
		SnarkGnome snarkgnome = new SnarkGnome();
		snarkgnome.args = args;

		// Initialize the Snark in a separate thread
		Thread thread = new Thread(snarkgnome);
		thread.start();

		// Go! Handle events...
		Gtk.main();
	}

	// True when progress() is called, cleared when madeProgress() is called.
	// Change/Check this flag only while holding the lock on this.
	private boolean progress = false;

	private synchronized void progress() {
		progress = true;
	}

	public void peerChange(PeerCoordinator coordinator, Peer peer) {
		activity = getActivity();
		progress();
	}

	public void storageCreateFile(Storage storage, String name, long length) {
		// TODO We should display something about this...
		activity = ALLOCATING;
	}

	// How much storage space has been allocated
	private long allocated = 0;

	public void storageAllocated(Storage storage, long length) {
		activity = ALLOCATING;
		allocated += length;
		if (allocated == Snark.meta.getTotalLength())
			; // We are done, but we done't care
		progress();
	}

	// How many pieces have been checked.
	private int checked = 0;

	boolean prechecking = true;

	public void storageChecked(Storage storage, int num, boolean checked) {
		if (prechecking)
			activity = CHECKING;
		else {
			// TODO Should we display something about BAD pieces?
			activity = getActivity();
		}
		this.checked++;
		progress();
	}

	public void storageAllChecked(Storage storage) {
		prechecking = false;
		activity = getActivity();
	}

	/**
	 * Returns the current activity by checking the storage and peer
	 * coordinator.
	 */
	private String getActivity() {
		String activity;

		// No turning back from a shutdown
		if (this.activity == SHUTDOWN)
			return SHUTDOWN;

		if (Snark.coordinator != null && Snark.coordinator.peers != null) {
			synchronized (Snark.coordinator.peers) {
				if (Snark.coordinator.peers.size() > 0
						&& (Snark.coordinator.getDownloaded() > 0 || Snark.coordinator
								.getUploaded() > 0)) {
					if (Snark.storage != null && Snark.storage.complete())
						activity = SHARING;
					else
						activity = COLLECTING;
				} else
					activity = CONNECTING;
			}
		} else
			activity = CONNECTING;

		return activity;
	}

	/**
	 * Called by fire() to check if there was any progress recently. Returns
	 * true when progress() was called since the last call to madeProgress().
	 * Synchronized to make sure we don't miss any progress events.
	 */
	private synchronized boolean madeProgress() {
		boolean result = progress;
		progress = false;
		return result;
	}

	// Used for keeping track of the up and download rate
	// Updated every second in fire().
	private long lastDownloaded = 0;
	private long downb = 0;
	private long lastUploaded = 0;
	private long upb = 0;

	private static final String STARTUP = "Starting up";
	private static final String ALLOCATING = "Creating files";
	private static final String CHECKING = "Checking files";
	private static final String CONNECTING = "Connecting to peers";
	private static final String COLLECTING = "Collecting pieces";
	private static final String SHARING = "Sharing pieces";
	private static final String SHUTDOWN = "Shutting down";
	
	private String activity = STARTUP;
	private String lastActivity = null;

	/**
	 * Sets upload and download rates texts. Used for everything gtk+/gnome
	 * since that seems the most thread save way.
	 */
	public boolean fire() {

		// What are we doing?
		if (activity != lastActivity) {
			// Update status text and reset progress bar
			appbar.setStatusDefault(activity);
			ProgressBar progressBar = appbar.getProgressBar();
			progressBar.setFraction(0);
			progressBar.setText("");
			lastActivity = activity;
			progress();
		}

		// Do we know the torrent name we are sharing now?
		if (Snark.meta != null && name == null) {
			name = Snark.meta.getName();
			torrentName.setText(name);
		}

		// Calculate and update download and upload speeds
		if (Snark.coordinator != null
				&& (activity == COLLECTING || activity == SHARING)) {
			// Calculate and show download rate.
			long downloaded = Snark.coordinator.getDownloaded();
			long diff = downloaded - lastDownloaded;
			downb -= downb / 10;
			downb += diff / 10;
			long kb = downb / (1024 / UPDATE_TIMER);
			String totalDown;
			if (downloaded >= (10 * 1024 * 1024))
				totalDown = (downloaded / (1024 * 1024)) + " MB";
			else
				totalDown = (downloaded / 1024) + " KB";

			downloadRate.setText(kb + " KB/s (" + totalDown + ")");
			lastDownloaded = downloaded;

			// Calculate and show upload rate.
			long uploaded = Snark.coordinator.getUploaded();
			diff = uploaded - lastUploaded;
			upb -= upb / 10;
			upb += diff / 10;
			kb = upb / (1024 / UPDATE_TIMER);
			String totalUp;
			if (uploaded >= (10 * 1024 * 1024))
				totalUp = (uploaded / (1024 * 1024)) + " MB";
			else
				totalUp = (uploaded / 1024) + " KB";

			uploadRate.setText(kb + " KB/s (" + totalUp + ")");
			lastUploaded = uploaded;
		}

		// Did we make progress?
		boolean update = madeProgress();
		if (Snark.meta != null && Snark.storage != null && update) {
			// Pieces and percentagees
			int pieces = Snark.meta.getPieces();
			int needed = Snark.storage.needed();
			int got = pieces - needed;

			long percentage;
			if (activity == ALLOCATING)
				percentage = (100 * allocated) / Snark.meta.getTotalLength();
			else if (activity == CHECKING)
				percentage = (100 * checked) / pieces;
			else
				percentage = (100 * got) / pieces;

			// Update progress bar
			double progress = percentage / 100d;
			ProgressBar progressBar = appbar.getProgressBar();

			if (activity == ALLOCATING || activity == CHECKING
					|| activity == COLLECTING) {
				progressBar.setFraction(progress);
				progressBar.setText(percentage + "%");
			} else
				progressBar.pulse();

			// Update collected pieces count
			piecesCollected.setText(got + " of " + pieces);

			if (Snark.coordinator != null)
				propertiesWindow.update(Snark.coordinator.getPeers());
		}

		// We want to run again and again and again...
		return true;
	}

}
