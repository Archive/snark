#/bin/sh

JAVA=gij
CLASSPATH=/usr/share/java/gtk2.6.jar:/usr/share/java/gnome2.10.jar:/usr/share/java/glade2.8.jar
MAINCLASS=org.klomp.snark.SnarkGnome

${JAVA} -classpath ${CLASSPATH} ${MAINCLASS} $*
